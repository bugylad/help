# GRUB REMEMBER LAST ENTRY

I want grub to remember the last OS i used.

## EDIT `/etc/default/grub`

```bash
sudo nano /etc/default/grub
```


Make sure these lines are in the file:

- **GRUB_DEFAULT=saved**
- **GRUB_SAVEDEFAULT=true**

## UPDATE GRUB

```bash
sudo update-grub
```

### If update-grub command doesn't work:

```bash
sudo grub2-mkconfig -o /etc/grub2.cfg
sudo grub2-mkconfig -o /etc/grub2-efi.cfg
```
